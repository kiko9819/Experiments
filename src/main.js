function toggleMenu(menu) {
  menu.classList.toggle('hidden');
}
function toggleCars() {
  const dropDownBtn = document.querySelector('.container');
  const dropDownMenu = document.querySelector('.drop-down');

  dropDownBtn.addEventListener('click', function() {
    toggleMenu(dropDownMenu);
  });
}
toggleCars();
